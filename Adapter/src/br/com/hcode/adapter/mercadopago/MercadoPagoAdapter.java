package br.com.hcode.adapter.mercadopago;

import br.com.hcode.adapter.paypal.IPayPalPayments;
import br.com.hcode.adapter.utils.Token;

public class MercadoPagoAdapter implements IPayPalPayments {
    private MercadoPago mercadoPago;
    public MercadoPagoAdapter(final MercadoPago mercadoPago) {
        this.mercadoPago = mercadoPago;
    }

    private Token token;
    @Override
    public Token authToken() {
        return new Token();
    }

    @Override
    public void paypalPayment() {
        mercadoPago.autenticarPagamento(authToken().toString());
        mercadoPago.efetuarPagamento();
        mercadoPago.gerarExtrato();
    }

    @Override
    public void paypalReceive() {
        mercadoPago.receberPagamento();
        mercadoPago.notificarCliente();
    }
}
