package br.com.hcode.adapter.mercadopago;

public interface IMercadoPago {
    void efetuarPagamento();
    void receberPagamento();
    void autenticarPagamento(String token);
    void gerarExtrato();
    void notificarCliente();

}
