package br.com.hcode.adapter.mercadopago;

public class MercadoPago implements IMercadoPago{

    @Override
    public void efetuarPagamento() {
        System.out.println("Pagamento efetuado via Mercado Pago");
    }

    @Override
    public void receberPagamento() {
        System.out.println("Pagamento recebido via Mercado Pago");
    }

    @Override
    public void autenticarPagamento(String token) {
        System.out.println("Pagamento autenticado via Mercado Pago");
    }

    @Override
    public void gerarExtrato() {
        System.out.println("Extrato gerado via Mercado Pago");
    }

    @Override
    public void notificarCliente() {
        System.out.println("Cliente notificado via Mercado Pago");
    }
}
